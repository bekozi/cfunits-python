.. cfunits-python documentation master file, created by
   sphinx-quickstart on Wed Aug 3 16:28:25 2011.  You can adapt this
   file completely to your liking, but it should at least contain the
   root `toctree` directive.

.. currentmodule:: cfunits
.. default-role:: obj

cfunits-python |release| documentation
======================================

.. toctree::
   :maxdepth: 3
   
   introduction

----

.. toctree::
   :maxdepth: 1

   installation

----

.. toctree::
   :maxdepth: 2

   cfunits.Units

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

